export const API = 'https://task-control-app.herokuapp.com';
export const API_PATH_BOARD = 'api/board';
export const API_PATH_COLUMN = 'api/columns';
export const API_PATH_TASK = 'api/tasks';
export const API_PATH_AUTH = 'api/auth';
export const PATH_LOGIN = 'login';
export const PATH_REGISTER = 'register';
export const PATH_HOME = 'home';

